import React from 'react';
import style from './header.module.css'

function Header(props) {
  return (
    <div className={style.header}>
      <h1>Movies</h1>
    </div>
  );
}

export default Header;