import React, { Component } from 'react';
import {connect} from 'react-redux'
import {searchAction} from '../../redux/actions/search'
import style from './movieslist.module.css'
import axios from 'axios'

class MoviesList extends Component {
  constructor() {
    super()
    this.state = {
      genres : []
    }
  }

  componentDidMount() {
    axios.get("https://api.themoviedb.org/3/genre/movie/list?api_key=ba250eba3387ac7556dba54aee93d992&language=pt-BR")
    .then(response => this.setState({genres: response.data.genres}))
  }

  onSearch = event => {
    const query = event.target.value
    this.props.searchAction(query) 
  }
  formatDate = date => {
    const datereg = /(\d{4})-(\d{1,2})-(\d{1,2})/
    return date.replace(datereg, '$3/$2/$1')
  }

  filterGenre = genre => {
    const filtered = this.state.genres.filter( genra => genre.includes(genra.id))
    console.log("FILTERED", filtered)
    return filtered.map(genra => <p className={style.genreTag}>{genra.name}</p>)
  }
  render() {
    return (
      <div className={style.listWrapper}>
        <input type="text" onChange={this.onSearch} />
        {this.props.movies.map(movie => (
          <div className={style.movie}>
            <img src={`https://image.tmdb.org/t/p/w200/${movie.poster_path}`} alt={`poster do filme ${movie.title}`} className={style.imageSize}/>
            <div className={style.movieInfo}>
              <h3>{movie.title}</h3>
              <div>
                <div className={style.smallInfo}>
                  <p>{movie.vote_average}</p>
                  <p>{this.formatDate(movie.release_date)}</p>
                </div>
                <p>{movie.overview}</p>
                <div className={style.genreContainer}>
                  {this.filterGenre(movie.genre_ids)}
                </div>
              </div>
            </div>
          </div>
        ))}
        
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    movies: state? state.movies.results : [],
    page: !!state && state.movies.page,
    totalPages: !!state && state.movies.total_pages
  }
}

export default connect(mapStateToProps, {searchAction})(MoviesList);