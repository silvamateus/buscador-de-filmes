import React from 'react';
import {Provider} from 'react-redux'
import store from './redux/store' 
import './App.css';
import Header from './components/Header'
import MoviesList from './components/MoviesList'

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Header />
        <MoviesList />
      </div>
    </Provider>
  );
}

export default App;
