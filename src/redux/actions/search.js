import axios from 'axios'
import {MOVIE_SEARCH, NO_MOVIE_SEARCH} from './types';

const fetchMovies = query => axios.get(`https://api.themoviedb.org/3/search/movie?api_key=ba250eba3387ac7556dba54aee93d992&language=pt-BR&query=${query}&append_to_response=images&include_image_language=pt-BR,null`)

export const searchAction = query => dispatch => fetchMovies(query)
.then(response => dispatch({
  type: MOVIE_SEARCH,
  payload: response.data
}))
.catch(err => dispatch({
  type: NO_MOVIE_SEARCH,
  error: err
}))
