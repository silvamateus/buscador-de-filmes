import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import moviesReducer from '../reducers';

const initialState = {
  page: 1,
  result: [],
  total_pages: 0
}

const store = createStore(moviesReducer, initialState, applyMiddleware(thunk))

export default store