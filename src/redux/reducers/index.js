import {MOVIE_SEARCH }from '../actions/types'

const moviesReducer = (state, action) => {

  if(action.type === MOVIE_SEARCH) {
    return {
      ...state,
      movies: action.payload
    }
  }

}

export default moviesReducer